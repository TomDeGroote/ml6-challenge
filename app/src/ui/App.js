import React, { Component } from "react";
import Button from "./components/buttons/Button";
import styled from "styled-components";
import axios from "axios";
import LoadingSvg from "./components/svgs/LoadingSvg";

const AppWrapper = styled.div`
  background-color: #282c34;
  min-height: 100vh;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

const TextWrapper = styled.div`
  max-height: 80vh;
  overflow-y: auto;
  padding: 0 5%;
`;

const Text = styled.p`
  margin: 8px;
  font-size: ${props => props.size};
  color: white;
  cursor: default;
`;

const Svg = styled.svg`
  stroke: #61dafb;
  width: 50px;
  height: 50px;
  animation: spin infinite 0.8s linear;

  @keyframes spin {
    from {
      transform: rotate(0deg);
    }
    to {
      transform: rotate(360deg);
    }
  }
`;

/**
 * App is the root component of this application. Depending on its state the user is
 * represented with:
 *    a. an upload button to upload a .wav file
 *    b. a loading icon when the .wav file is being processed
 *    c. the text of the .wav file when the .wav file has been processed.
 */
class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      text: undefined,
      loading: false
    };
  }

  render() {
    // based on the text and loading values in state
    // a inner component is chosen to represent
    let inner;
    if (this.state.loading) {
      // if loading show the loading icon
      inner = this.getLoading();
    } else if (this.state.text) {
      // if text has some value, show the text
      inner = this.getShowText();
    } else {
      // default show the upload button
      inner = this.getUpload();
    }

    return <AppWrapper className="app-wrapper">{inner}</AppWrapper>;
  }

  // returns the upload button of type file-upload
  getUpload() {
    return (
      <React.Fragment>
        <Button type="file-upload" action={() => this.handleFileUpload()} />
        <Text size="14px">upload .wav</Text>
      </React.Fragment>
    );
  }

  // returns the text as wel as a button of type back, when back is pressed
  // the text in state is reset
  getShowText() {
    return (
      <React.Fragment>
        <Button type="back" action={() => this.setState({ text: "" })} />
        <TextWrapper>
          <Text size="50px">{this.state.text}</Text>
        </TextWrapper>
      </React.Fragment>
    );
  }

  // returns the loading icon
  getLoading() {
    return (
      <Svg>
        <LoadingSvg />
      </Svg>
    );
  }

  // handles file uploads
  handleFileUpload() {
    const files = document.getElementById("file-upload").files;

    if (files.length === 1) {
      const file = files[0];
      if (file.type !== "audio/wav" && file.type !== "audio/x-wav") {
        // alert the user to use a .wav file if necessary
        alert("please upload a .wav audio file");
      } else {
        // put the file in formData and send to the server
        let formData = new FormData();
        formData.append("file", file);
        this.setState({ loading: true });
        axios
          .post("http://35.241.169.164/upload", formData, {
            headers: {
              "Content-Type": "multipart/form-data"
            }
          })
          .then(response => {
            // on 200, change the state accordingly
            this.setState({
              text: response.data,
              loading: false
            });
          })
          .catch(error => {
            // on error return, stop loading and alert the user something went wrong
            this.setState({
              loading: false
            });
            alert("something went wrong, please try again");
          });
      }
    } else {
      alert("please upload exactly one file");
    }
  }
}

export default App;
