import React, { Component } from "react";
import styled from "styled-components";
import UploadSvg from "../svgs/UploadSvg";
import BackSvg from "../svgs/BackSvg";

const HiddenInput = styled.input`
  display: none;
`;

const Circle = styled.div`
  position: relative;
  text-align: center;
  vertical-align: middle;

  width: ${props => props.size};
  height: ${props => props.size};
  border-radius: 50%;
  cursor: pointer;
  background-color: #61dafb;
  fill: white;

  &:hover {
    background-color: white;
    fill: #61dafb;
  }
`;

const Svg = styled.svg`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  width: 60%;
  height: 60%};
`;

const SvgButton = styled.svg`
  position: fixed;
  top: 10px;
  left: 10px;
  width: ${props => props.size};
  height: ${props => props.size};
  fill: white;

  &:hover {
    fill: #61dafb;
    cursor: pointer;
  }
`;

/**
 * The Button component can render buttons of type "file-upload" and "back".
 * In both cases an action can be provided for when the button is pressed (case type "back")
 * or for what to do with the selected file (case type "file-upload").
 * 
 * @property type: the type of button to render: "file-upload" or "back"
 * @property action: function to execute when the file is uploaded or back-button is pressed
 */
export default class Button extends Component {

  render() {
    const { type, action } = this.props;

    // use the correct button based on the given type
    switch (type) {
      case "file-upload":
        return this.getFileUploadButton(action);
      case "back":
        return this.getBackButton(action);
      default:
        console.log("WARNING: invalid button type provided")
    }
  }

  // returns a file-upload button
  getFileUploadButton(action) {
    return (
      <React.Fragment>
        <label htmlFor="file-upload">
          <Circle size="50px">
            <Svg>
              <UploadSvg />
            </Svg>
          </Circle>
        </label>
        <HiddenInput id="file-upload" type="file" onChange={() => action()} />
      </React.Fragment>
    );
  }

  // returns a back button
  getBackButton(action) {
    return (
      <SvgButton size="40px" onClick={() => action()}>
        <BackSvg />
      </SvgButton>
    );
  }
}
