import React, { Component } from "react";

export default class LoadingSvg extends Component {
  render() {
    return (
      <svg viewBox="0 0 6.3 6.3">
        <g transform="translate(0,-290.64998)">
          <path
            style={{ fill: "none", strokeWidth: 0.565 }}
            d="m 3.6448384,296.29534 c -1.3456869,0.25594 -2.68426511,-0.65483 -2.94020137,-2.00052 -0.25593627,-1.34569 0.65483757,-2.68426 2.00052447,-2.9402 1.3456869,-0.25594 2.6842651,0.65484 2.9402014,2.00052 0.049214,0.25877 0.057437,0.52508 0.02428,0.78638"
          />
        </g>
      </svg>
    );
  }
}
