# ML6 Challenge

## api
De api is terug te vinden in de 'api' folder. Deze api is een wrapper rond de Google's speech API geschreven met Python. De api draait op dit moment op Kubernetes en .wav bestanden kunnen rechtstreeks worden geupload via http://35.241.169.164/upload.

## app
Met behulp van ReactJS is een front-end geschreven die gebruik maakt van de hierboven beschreven API. De front-end draait ook op Kubernetes en is te bereiken via http://146.148.127.190/.