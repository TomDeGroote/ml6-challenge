import flask
from flask import Flask, request
from flask_cors import CORS, cross_origin

import base64
import time

# [START speech_quickstart]
import io
import os

# Imports the Google Cloud client library
from google.cloud import speech
from google.cloud.speech import enums
from google.cloud.speech import types

app = flask.Flask(__name__)
cors = CORS(app)


def get_text_from_file(audio_file):

    # Instantiates a client
    client = speech.SpeechClient()

    # load audio file and config
    content = audio_file.read()
    audio = types.RecognitionAudio(content=content)
    config = types.RecognitionConfig(
        encoding=enums.RecognitionConfig.AudioEncoding.LINEAR16,
        language_code='en-US')

    # Detects speech in the audio file
    response = client.recognize(config, audio)

    text = ""
    for result in response.results:
        text += result.alternatives[0].transcript

    return text


@app.route('/upload', methods=['POST'])
@cross_origin()
def upload_file():
    if request.method == 'POST':
        # get the file from the request
        file = request.files['file']
        result = get_text_from_file(file)
        return result


if __name__ == '__main__':
    app.run(host='0.0.0.0')
